import React, { Component } from 'react';
import logo from './logo.svg';
import { Card } from './components/card/card.jsx'
import { Counter } from './components/counter/Counter'
import { Match } from './components/match/match'
import './App.css';

class App extends Component {
  constructor() {
    super()

    this.state = {
      posts: [],
      titleArray: ['Mon titre 1', 'Mon titre 2', 'Mon titre 3'],
      titleIndex: 0,
      title: 'Titre de base'
    }
  }

  changeTitle = () => {
    this.setState({ title: 'mon nouveau titre' })
  }

  changeIndexTitle = () => {
    this.setState({ titleIndex: this.state.titleIndex += 1 })
  }

  componentDidMount() {
    fetch('https://jsonplaceholder.typicode.com/posts')
    .then( response => response.json() )
    .then( posts => this.setState({ posts: posts }) )
  }

  render () {
    const { posts } = this.state

    return (
      <div className="App">
        <main className='main'>
          <h1 className='title-array'>{ this.state.titleArray[this.state.titleIndex] }</h1>

          <button onClick={ this.changeIndexTitle }> Changer le titre </button>

          <Match teamOne='Saint Etienne' teamTwo='Lyon' />

          <div className="archive">
            {
              posts.map( ({ id, ...otherProps }) => (
                <Card key={ id } { ...otherProps }/>
              ))
            }
          </div>
        </main>
      </div>
    );
  }
}

export default App;
