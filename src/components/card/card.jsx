import React from 'react'

import './card.css'

export const Card = ({ title, body }) => (
    <div className='card'>
        <h3> { title } </h3>
        <p> { body } </p>
    </div>
)
