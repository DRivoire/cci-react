import React, { Component } from 'react'
import './counter.css';

class Counter extends Component {
    constructor() {
        super()

        this.state = {
            counter: 0,
        }
    }

    increment = () => {
        this.setState((prevState) => ({
            counter: prevState.counter + 1
        }));
    }

    decrement = () => {
        this.setState((prevState) => ({
            counter: prevState.counter - 1
        }));
    }

    reset = () => {
        this.setState(() => ({
            counter: 0
        }));
    }

    render() {
        return (
            <div className="counter">
                <p className="counter-content"> { this.state.counter } </p>

                <button onClick={ this.increment } className="counter-button"> + </button>
                <button onClick={ this.decrement } className="counter-button"> - </button>
                <button onClick={ this.reset } className="counter-button"> Reset match </button>
            </div>
        )
    }
}

export {
    Counter
}