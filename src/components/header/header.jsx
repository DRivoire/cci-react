import React from 'react'
import { Link } from 'react-router-dom'

export const Header = () => (
    <header className='header'>
      <Link to="/">Accueil</Link>
      <Link to="/teams">Equipes</Link>
    </header>
)