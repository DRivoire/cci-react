import React from 'react'
import { Counter } from '../counter/Counter'

import './match.css'

export const Match = ( { teamOne, teamTwo } ) => (
    <div className='match'>
        <h2 className='match-title'> Mon match </h2>

        <div className="team">
            <h2 className="team-title"> { teamOne } </h2>
            <Counter />
        </div>

        <div className="team">
            <h2 className="team-title"> { teamTwo } </h2>
            <Counter />
        </div>
    </div>
)
