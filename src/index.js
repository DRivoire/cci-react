import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import App from './App';
import { Teams } from './pages/teams/teams';
import { Header } from './components/header/header'
import './index.css';

ReactDOM.render(
<BrowserRouter>    
    <Header/>

    <Routes>
      <Route path="/" element={<App />} />
      <Route path="/teams" element={<Teams />} />
    </Routes>
  </BrowserRouter>,
  document.getElementById('root')
);

